import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
    
    render(){
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to="/" className="navbar-brand">ExerTracker</Link>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <Link to="/" className="nav-link">Exercicios</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/create" className="nav-link">Criar registro de exercício</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/user" className="nav-link">Criar usuário</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
};